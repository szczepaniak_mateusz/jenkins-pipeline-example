#!/usr/bin/env ruby

environment_name = ARGV[0]

counter=0
timeout=40

while counter <= timeout
  status = `aws elasticbeanstalk describe-environments --environment-name #{environment_name} | jq .Environments[0].Status`.strip
  puts "Checking environment status: #{status}"

  if status == '"Ready"'
    break
  elsif status == '"Updating"'
    sleep(10)
  else
    puts "ERROR: Environment status: #{status}"
    exit 1
  end

  if counter == timeout
    puts "ERROR: Environment status: #{status}"
    exit 1
  end

  counter = counter + 1
end

counter=0

while counter <= timeout
  health = `aws elasticbeanstalk describe-environments --environment-name #{environment_name} | jq .Environments[0].Health`.strip
  puts "Checking environment health: #{health}"

  if health == '"Green"'
    break
  elsif health == '"Red"'
    puts "ERROR: Environment health: #{health}"
    exit 1
  else
    sleep(10)
  end

  if counter == timeout
    puts "ERROR: Environment health: #{health}"
    exit 1
  end

  counter = counter + 1
end

exit 0
