
// Example pipeline definition. It creates 3 jobs in Jenkins for dev, int, prod environment.

def package_directory = "example-project-package"

// Amazon ElasticBeanstalk application name
def aws_app = "example-project.polaris.no"

// Amazon environments
def aws_env_dev = "example-project-dev"
def aws_env_int = "example-project-int"
def aws_env_prod = "example-project-prod"

// Jenkins jobs names
def job_dev = "example-project-dev"
def job_int = "example-project-int"
def job_prod = "example-project-prod"

job(job_dev) {
    logRotator {
        numToKeep(3)
    }
    scm {
        git {
            remote {  url("https://bitbucket.org/polarismedia/example-project.git")  }
            branch("master")
        }
    }
    wrappers {
        deliveryPipelineVersion('${GIT_REVISION,length=7}-${BUILD_NUMBER}', true)
    }
    steps {
        // Build
        shell("mvn -e clean install")

        // Remove version if exists
        shell("aws elasticbeanstalk delete-application-version --application-name \"${aws_app}\" --version-label \"\${PIPELINE_VERSION}\" --delete-source-bundle")

        // Upload new version
        shell("aws s3 cp ${package_directory}/target/${package_directory}.zip s3://\${AWS_S3_BUCKET_NAME}/${aws_app}-\${PIPELINE_VERSION}.zip")

        // Create new version
        shell("aws elasticbeanstalk create-application-version --application-name \"${aws_app}\" --version-label \"\${PIPELINE_VERSION}\" --source-bundle S3Bucket=\"\${AWS_S3_BUCKET_NAME}\",S3Key=\"${aws_app}-\${PIPELINE_VERSION}.zip\"")

        // Deploy new version
        shell("aws elasticbeanstalk update-environment --environment-name \"${aws_env_dev}\" --version-label \"\${PIPELINE_VERSION}\"")

        // Wait until application is healthy
        shell("ruby \${JENKINS_PIPELINES_WORKSPACE}/scripts/aws_elasticbeanstalk_wait_for_environments.rb \"${aws_env_dev}\"")

        // Run your integration tests here
    }
    publishers {
        archiveArtifacts("**/target/*.zip")
        fingerprint("**/target/*.zip")
        mailer('$DEFAULT_RECIPIENTS', true, false)
        downstream(job_int, 'SUCCESS')
    }
}

context.job(job_int) {
    blockOnUpstreamProjects()
    logRotator {
        numToKeep(3)
    }
    steps {
        copyArtifacts(job_dev) {
            includePatterns('**/*.zip')
            flatten()
            fingerprintArtifacts(true)
            buildSelector {
                buildNumber("\$PIPELINE_VERSION")
            }
        }
        // Deploy new version
        shell("aws elasticbeanstalk update-environment --environment-name \"${aws_env_int}\" --version-label \"\${PIPELINE_VERSION}\"")

        // Wait until application is healthy
        shell("ruby \${JENKINS_PIPELINES_WORKSPACE}/scripts/aws_elasticbeanstalk_wait_for_environments.rb \"${aws_env_int}\"")

        // Run your integration tests here
    }
    publishers {
        mailer('$DEFAULT_RECIPIENTS', true, false)
    }
    configure { project ->
        project / publishers / 'au.com.centrumsystems.hudson.plugin.buildpipeline.trigger.BuildPipelineTrigger'(plugin: 'build-pipeline-plugin@1.4.8') {
            'downstreamProjectNames' job_prod
        }
    }
}

job(job_prod) {
    blockOnUpstreamProjects()
    logRotator {
        numToKeep(10)
    }
    steps {
        copyArtifacts(job_dev) {
            includePatterns('**/*.zip')
            flatten()
            fingerprintArtifacts(true)
            buildSelector {
                buildNumber("\$PIPELINE_VERSION")
            }
        }
        // Deploy new version
        shell("aws elasticbeanstalk update-environment --environment-name \"${aws_env_prod}\" --version-label \"\${PIPELINE_VERSION}\"")

        // Wait until application is healthy
        shell("ruby \${JENKINS_PIPELINES_WORKSPACE}/scripts/aws_elasticbeanstalk_wait_for_environments.rb \"${aws_env_prod}\"")

        // Run your integration tests here
    }
    publishers {
        mailer('$DEFAULT_RECIPIENTS', false, false)
    }
}