# Jenkins Pipeline Example Project  

How to use it?

 - Create in jenkins job ``jenkins-pipelines`` from this git repository.

 - In Job DSL plugin set path to ``inception/*.groovy``

 - Configure ``example.groovy`` file to match you amazon application settings
 
 - Configure in Jenkins env variables: ``AWS_KEY, AWS_S3_BUCKET_NAME, AWS_SECRET, DEFAULT_RECIPIENTS, JENKINS_PIPELINES_WORKSPACE``
   * ``JENKINS_PIPELINES_WORKSPACE`` - /var/lib/jenkins/jobs/jenkins-pipelines/workspace
 
### Jenkins jobs naming convention

``{project}--{env}``

* ``project`` - project name
* ``env`` - one of: dev, int, prod

Example: ``example--dev``

